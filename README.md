# Compiling

The *Makefile* is in the main folder, so simply execute *make*:

    make

The output binary is *provenance*.

## Dependencies

The code needs the Boost C++ library headers, specifically the Fibonacci
heap implementation and *boost_program_options*. It assumes the include files are present in
*/usr/local/include*. If your Boost installation is someplace else, you have to
modify the *INCLUDE_DIRS* directive in *Makefile*. The binary library does not
need to be linked.

# Usage

## Input Formats

The program expects as input a graph in the following format, one edge per line:
    
    node1 <TAB> node2

where *node1* and *node2* are the endpoints of the edge.

Depending on the semiring, other supporting files are expected.

### k-NN Semiring

For the k-NN semiring each graph file should have a corresponding
*graph_name.knn* file containing lines of the form:
    
    node1 <TAB> node2
    n
    edge1
    ...
    edgen
    len
    desc1
    ...
    desclen

where *node1* and *node2* are the endpoints of the edge, *n* is the number of edges, *edge1* to *edgen* are the weight of the edges, *len* is the length of the description of the edge, and *desc1* to
*desclen* contains are lines containing the description.

### Tropical Semiring

For the tropical semiring each graph file should have a corresponding
*graph_name.tropical* file containing lines of the form:
    
    node1 <TAB> node2
    dist

where *node1* and *node2* are the endpoints of the edge, and *dist* is the
weight of the edge.


## Creating The Triangulation

The first step is to create a triangulation using the following command:

    ./treedecomp --decompose <graph_file> [<method>]

The parameters are:

* *graph_file* is the name of the graph file
* *method* is the criteria by which each node is chosen next in the
  decomposition sequence, and can have the following values: **0** minimum
  degree (default), **1** minimum fill in, and **2** minimum degree+fill in

### Output

The output will be a file with the name *<graph>.<method>.dec* containing the
resulting ordering and the pre-computed triangulation provenance circuits.

## Answering Queries

The command to answer a provenance query is:

    ./provenance query -g <graph_file> -s <src> -t <tgt> [<method>]

The parameters are:

* *graph_file* is the name of the graph file
* *method* is the criteria by which each node is chosen next in the
  decomposition sequence, and can have the following values: **0** minimum
  degree (default), **1** minimum fill in, and **2** minimum degree+fill in
  
Type *./provenance query* for usage.
    

# License

The source code is provided as-is under an MIT License.
