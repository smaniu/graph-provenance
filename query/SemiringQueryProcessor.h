//
//  SemiringQueryProcessor.h
//  graph-provenance
//
//  Created by Silviu Maniu on 30/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef SemiringQueryProcessor_h
#define SemiringQueryProcessor_h

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/heap/fibonacci_heap.hpp>
#include <vector>

#include "TriangulationQuery.h"
#include "../structure/TreeDecomposition.h"
#include "../semiring/Semiring.h"
#include "../semiring/DivisorSemiring.h" //needs to be removed at some time
#include "../strategy/PermutationStrategy.h"

#define	QUERY_ALGORITHM_MOHRI		0
#define QUERY_ALGORITHM_MINPATH		1
#define QUERY_ALGORITHM_NODELIM		2
#define QUERY_ALGORITHM_DIJKSTRA	3
#define QUERY_ALGORITHM_PRECOMP		4
#define QUERY_ALGORITHM_EXT_DIJKSTRA	5
#define QUERY_ALGORITHM_ACCESS	6

template<typename T> class SemiringQueryProcessor {
private:
	TreeDecomposition& dec;
	PermutationStrategy& str;
	std::unordered_map<unsigned long, std::unordered_map<unsigned long, \
		std::shared_ptr<T>>> annotations;
	std::unordered_map<unsigned long, std::unordered_map<unsigned long, \
		std::shared_ptr<T>>> comp;
	std::unordered_map<unsigned long, std::shared_ptr<T>> computed;
	std::string graph_file;
	int k;
	bool undirected;
	unsigned long edges = 0;
	unsigned long hits = 0;
	unsigned long misses = 0;

public:
	SemiringQueryProcessor(TreeDecomposition& dec, std::string graph_file, PermutationStrategy& str,\
		int k = 0, bool undirected = true) :\
		dec(dec), str(str), graph_file(graph_file), undirected(undirected), k(k) {
	}

	void init() {
		//reading annotations from the graph
		std::stringstream g_in;
		g_in << graph_file << "." << T::get_label();
		std::ifstream file_graph(g_in.str());

		if (!file_graph) {
			std::cerr << "Annotation file " << g_in.str() << " not found." << std::endl;
			exit(1);
		}

		unsigned long src, tgt;
		while (file_graph >> src >> tgt) {
			typename T::value_type val;
			file_graph >> val;
			if (src != tgt) {
				std::shared_ptr<T> ptr_annot(new T(val,k));
				annotations[src][tgt] = ptr_annot;
				if (undirected) annotations[tgt][src] = ptr_annot;
			} else {
				std::shared_ptr<T> ptr_annot(new T(val,k));
				annotations[src][tgt] = ptr_annot;
			}
		}
		file_graph.close();
	}

	/*
	 precomputes triangulated provenances
	*/
	void precomputeProvenances() {
		computed.clear();
		for (auto prov : dec.getCircuits())
			answerProvenance(prov);
	}

	/*
	 precompute the bags directly
	*/
	void precomputeBags() {
		edges = 0;
		comp = annotations;
		for (auto bag : dec.getBags())
			computeBag(bag);
		std::cerr << "DEBUG: " << edges << " precomputed edges ";
	}


	std::shared_ptr<T> answer(unsigned long src, unsigned long tgt, int algorithm) {
		std::shared_ptr<T> ans = nullptr;
		TriangulationQuery qry;
		switch (algorithm) {
		case QUERY_ALGORITHM_MOHRI:
			ans = answerOnMohri(src, tgt);
			break;
		case QUERY_ALGORITHM_MINPATH:
			ans = answerOnTriangulation(src, tgt);
			break;
		case QUERY_ALGORITHM_NODELIM:
			ans = answerOnNodeElim(src, tgt);
			break;
		case QUERY_ALGORITHM_DIJKSTRA:
			ans = answerOnDijkstra(src, tgt);
			break;
		case QUERY_ALGORITHM_PRECOMP:
			qry.preparePairQuery(dec, src, tgt);
			ans = answerFromPreparedProvenance(qry);
			break;
		case QUERY_ALGORITHM_EXT_DIJKSTRA:
			ans = answerOnExtendedDijkstra(src, tgt);
			break;
		case QUERY_ALGORITHM_ACCESS:
			ans = answerAccess(src, tgt);
			break;
		default:
			std::cerr << "Unknown algorithm." << std::endl;
			exit(1);
		}
		return ans;
	}


private:

	/*
	 answer directly on the prepared provenance circuit (DOES NOT WORK WITH CYCLES)
	*/
	std::shared_ptr<T> answerFromPreparedProvenance(const TriangulationQuery& qry) {
		edges = 0; hits = 0; misses = 0;
		computed.clear();
		auto res = answerProvenance(qry.provPtr());
		std::cerr << "DEBUG: edges=" << edges << " misses=" << misses << " hits="\
			<< hits << std::endl;
		return res;
	}

	/*
	 answers the query on the graph annotations using Node Elimination algorithm
	 need to be able to define properly a star over the semiring
	 (all k-closed have this property).
	 Only works well in the non oriented case for now
	 (need to change the structure of the graph).
	*/
	std::shared_ptr<T> answerOnNodeElim(unsigned long src, unsigned long tgt) {
		std::shared_ptr<T> one(new T(T::one(),k));
		std::shared_ptr<T> zero(new T(T::zero(),k));
		unsigned long former_size = annotations.size();
		annotations[former_size + 1][src] = one;
		annotations[src][former_size + 1] = one;
		annotations[tgt][former_size + 2] = one;
		annotations[former_size + 2][tgt] = one;
		//std::cout << former_size << " - " << annotations.size() << std::endl;
		//for (unsigned long i = 1; i <= former_size; i++) {
		while(!str.empty()){
			unsigned long i = str.get_next();
			//std::cout << "==== Studying node " << i << "====" << std::endl;
			std::unordered_map<unsigned long, std::unordered_map<unsigned long, \
				std::shared_ptr<T>>> annotations2(annotations);
			std::shared_ptr<T> star(new T(T::one(),k));
			if(annotations[i].find(i) != annotations[i].end()) {
                        	star = std::dynamic_pointer_cast<T>(annotations[i][i]->star());
                	        //std::cout<<"if.. star:\n"<< *star << "= ann[i][i] *\n"<< \
					*(annotations[i][i]) <<std::endl;
			}
			for (auto in : annotations[i]) {
				if(in.first == i) continue;
				for (auto out : annotations[i]) {
					if(out.first == i) continue;
                                        //std::cout << "----" << in.first << " to " << out.first << "----" <<std::endl;

					std::shared_ptr<T> newval(std::dynamic_pointer_cast<T>(annotations[in.first][i]));
					newval->setK(k);
					
					//std::cout<<"Checkpoint(1):\nnewval:\n" << *newval << "= ann[in.first][i]:\n" << *(annotations[in.first][i]) << std::endl;

					newval = std::dynamic_pointer_cast<T>(newval->times(std::dynamic_pointer_cast<T>(star)));
					//std::cout<<"if.. Checkpoint(3):\nnewval:\n"<< *newval << "= newval x star\n" <<std::endl;

					newval = std::dynamic_pointer_cast<T>(newval->times(std::dynamic_pointer_cast<T>(annotations[i][out.first])));
					//std::cout<< "Checkpoint(4):\nnewval:\n" << *newval << "= newval x ann[i][out.first]\n" \
						<< *(annotations[i][out.first]) << std::endl;

					if (annotations[in.first].find(out.first) != annotations[in.first].end()) {
                                                //std::cout<<"Checkpoint(5):\nnewval:\n" << *newval << "id" << std::endl;
						newval = \
						std::dynamic_pointer_cast<T>(annotations[in.first][out.first]->plus(newval));
					        //std::cout<<"Checkpoint(6)\nnewval:\n" << *newval << "= ann[in.first][out.first] + newval\n" \
						    << *(annotations[in.first][out.first]) <<std::endl;
					}
					//std::cout << "Checkpoint(7):\nnewval:\n" << *newval << std::endl;
					annotations2[in.first][out.first] = newval;
					//std::cout << in.first << " to " << out.first << std::endl;
					//std::cout << *newval << std::endl;
				}
			}
			for (auto nei : annotations[i]) {
				if (nei.first != i)
					annotations2[nei.first].erase(i);
			}
			annotations = annotations2;
		}
		if (annotations[former_size + 1].find(former_size + 2) != annotations[former_size + 1].end())
			return annotations[former_size + 1][former_size + 2];
		else
			return zero;
	}

	struct heap_type {
		unsigned long node;
		std::shared_ptr<T> element;

		bool operator<(const heap_type& a) const {
			//std::cout << "comp " << node << "-" << *element << " " << a.node << "-" << (*a.element);
			//bool res = (*element < (*a.element)) ? true : ((*a.element) < *element) ? false : node < a.node;
			//std::cout << "res: " << res << std::endl;
			return (*element < (*a.element)) ? true : ((*a.element) < *element) ? false : node < a.node;
		}
	};

	class heap_type_mohri {
	public:
		unsigned long node;
		Graph& g;
		PermutationStrategy& str;

		heap_type_mohri(unsigned long node, Graph& g, PermutationStrategy& str) : node(node), g(g), str(str) {}

		bool operator<(const heap_type_mohri& a) const {
			//std::cout << "comp " << node << "-" << *element << " " << a.node << "-" << (*a.element);
			//bool res = (*element < (*a.element)) ? true : ((*a.element) < *element) ? false : node < a.node;
			//std::cout << "res: " << res << std::endl;
			return (str.compute_statistic(node,g) < str.compute_statistic(a.node,g));
		}

		heap_type_mohri& operator=(const heap_type_mohri& other) {
			if (this != &other) {
				node = other.node;
				g = other.g;
				str = other.str;
			}
			return *this;
		}
	};

	/*
	 answers the query directly on the graph annotations using generalized
	 Dijkstra's algorithm; only works with 0-closed semiring having a total
	 natural order
	*/
	std::shared_ptr<T> answerOnDijkstra(unsigned long src, unsigned long tgt, std::shared_ptr<T> dimension = NULL) {
		std::shared_ptr<T> zero(new T(T::zero(),k));
		std::shared_ptr<T> one(new T(T::one(),k));
		boost::heap::fibonacci_heap<heap_type> heap;
		std::unordered_map<unsigned long,
			typename boost::heap::fibonacci_heap<heap_type>::handle_type> heap_nodes;
	std::cout << "annotations.size()=" << annotations.size() << " (annotations.begin())->first=" << (annotations.begin())->first << std::endl;

		std::vector<heap_type> heap_node;
		heap_node.resize((annotations.begin())->first + 1);
		std::vector<heap_type> heap_node2;
		heap_node2.resize((annotations.begin())->first + 1);
		heap_node[src].node = src;
		heap_node[src].element = one;
		heap_nodes[src] = heap.push(heap_node[src]);
		// touched vector necessary for implementing lazy init
		std::vector<unsigned long> touched;
		touched.resize((annotations.begin())->first  + 1, 0);
		touched[src] = 1;
	std::cout << annotations.size() << " " << (*heap_node[src].element) << std::endl;
	//if(dimension != NULL) std::cout << "Dimension is " << *dimension << std::endl;
		/*for (unsigned long i = 0; i <= annotations.size(); i++) {
			if (i != src) {
				heap_node[i].node = i;
				heap_node[i].element = zero;
			//std::cout << i << " " << (*heap_node[i].element) << std::endl;
				heap_nodes[i] = heap.push(heap_node[i]);
			}
		//if (i == src) std::cout << i << " " << (*heap_node[src].element) << std::endl;
		}*/
		std::vector<unsigned long> seen;
		seen.resize((annotations.begin())->first + 1, 0);
		while (heap.size() > 0) {
		std::cout << "deb while" << std::endl;
			heap_type node = heap.top();
		std::cout << "after top before pop" << std::endl;
				  //heap.pop();
		std::cout << "Seing node number " << node.node << " with value " << (*node.element) << std::endl;
			heap.pop();
		std::cout << "after pop" << std::endl;
			seen[node.node] = 1;
			if (node.node == tgt) return node.element;
			// implementation of a Mohri's like stopping condition
			if(node.element->getValue() == T::zero()) return zero;
			for (auto voisin : annotations[node.node]) {
				if (!seen[voisin.first]) {
					heap_node2[voisin.first].node = voisin.first;
				std::cout << " with value " << (*(*heap_nodes[voisin.first]).element);
				std::cout << " linked with " << (*annotations[node.node][voisin.first]) << std::endl;
					if(dimension == NULL) { // Dijkstra habituel
						std::shared_ptr<T> new_element_1 = std::dynamic_pointer_cast<T>(node.element->times(\
							std::dynamic_pointer_cast<T>(annotations[node.node][voisin.first])));
					std::cout << "test1" << std::endl;
						std::shared_ptr<T> new_element;
						if(touched[voisin.first]) {
							new_element = std::dynamic_pointer_cast<T>(
            							(*heap_nodes[voisin.first]).element->plus(new_element_1));
						} else {
							new_element = new_element_1;
						}
					std::cout << "test2" << std::endl;
						heap_node2[voisin.first].element = new_element;
					}
					else { // Dikstra suivant une dimension de la decomposition
                                          	std::shared_ptr<T> edge = std::dynamic_pointer_cast<T>(\
							annotations[node.node][voisin.first]->getSimpleValue(dimension));
					std::cout << "Filtred edge is " << *edge << std::endl;
					  	std::shared_ptr<T> new_element_1 = std::dynamic_pointer_cast<T>(node.element->times(\
							std::dynamic_pointer_cast<T>(edge)));
					std::cout << "New element1 is " << *new_element_1 << std::endl;
						std::shared_ptr<T> new_element;
						if(touched[voisin.first]) {
							new_element = std::dynamic_pointer_cast<T>(
								(*heap_nodes[voisin.first]).element->plus(new_element_1));
						} else {
							new_element = new_element_1;
						}
					std::cout << "New element is " << *new_element << std::endl;
						heap_node2[voisin.first].element = new_element;
					}
				std::cout << "New value for " << voisin.first << " is " << (*heap_node2[voisin.first].element) << std::endl;
					if(touched[voisin.first]) {
						auto handle = heap_nodes[voisin.first];
						heap.update(handle, heap_node2[voisin.first]);
					} else {
						touched[voisin.first] = 1;
						heap_nodes[voisin.first] = heap.push(heap_node2[voisin.first]);
					}
					heap_node[voisin.first] = heap_node2[voisin.first];
				std::cout << "We verify " << (*(*heap_nodes[voisin.first]).element) << std::endl;
				}
			}
		}
		return zero;
	}
	
	/*
	 answers the query directly on the graph annotations using the
	 extended version of Dijkstra's algorithm; 
	 only works for distributive lattices (0-closed and times-idempotent semirings)
	*/
	std::shared_ptr<T> answerOnExtendedDijkstra(unsigned long src, unsigned long tgt) {
		std::shared_ptr<T> rv(new T(T::zero(),k));
	        std::shared_ptr<DivisorSemiring> dv(new DivisorSemiring(0uL,0,false));
		for(const auto& value: dv->simpleElts) {
			//std::cout << "simpleElt: " << *value << std::endl;
			std::shared_ptr<T> rdij = answerOnDijkstra(src, tgt, std::dynamic_pointer_cast<T>(value));
			//std::cout << "Dijkstra's answer alongside the dimension of this elt: " << *rdij << std::endl;
					//*answerOnDijkstra(src, tgt, std::dynamic_pointer_cast<T>(value)) << std::endl;
			rv = std::dynamic_pointer_cast<T>(\
					rv->plus(std::dynamic_pointer_cast<T>(rdij)));
			
		}
		return rv;
	}

	 std::shared_ptr<T> answerAccess(unsigned long src, unsigned long tgt) {
		std::shared_ptr<T> zero(new T(T::zero(),k));
		std::shared_ptr<T> one(new T(T::one(),k));

		std::vector<unsigned long> seen;
		seen.resize(annotations.size() + 1, 0);
		
		std::queue<unsigned long> queue_nodes;
		queue_nodes.push(src);

		while(!queue_nodes.empty()) {
			unsigned long node = queue_nodes.front();
			queue_nodes.pop();
			seen[node] = 1;
			if (node == tgt) return one;
			for (auto voisin : annotations[node]) {
				if (!seen[voisin.first]) {
					queue_nodes.push(voisin.first);
				}
			}
		}
		return zero; 
	} 

	/*
	 answers the query directly on the graph annotations using Mohri's algorithm
	 only works with closed semirings (Tropical, Security, Boolean)
	*/
	std::shared_ptr<T> answerOnMohri(unsigned long src, unsigned long tgt) {
		edges = 0;
		std::shared_ptr<T> zero(new T(T::zero(),k));
		std::shared_ptr<T> one(new T(T::one(),k));
		std::unordered_map<unsigned long, std::shared_ptr<T>> prov;
		std::unordered_map<unsigned long, std::shared_ptr<T>> resid;
		std::unordered_set<unsigned long> queue_nodes;
		prov[src] = one;
		resid[src] = one;
		std::priority_queue<heap_type_mohri> q;
		heap_type_mohri ht_src ( src, dec.getGraph(), str );
		q.push(ht_src);
		queue_nodes.insert(src);
		//loop through the queue
		while (!q.empty()) {
			unsigned long node = q.top().node; q.pop(); queue_nodes.erase(node);
			std::shared_ptr<T> r = zero;
			if (resid.find(node) != resid.end()) r = resid[node];
			resid[node] = zero;
			//process every neighbour node
			if (annotations.find(node) != annotations.end()) {
				for (auto val : annotations[node]) {
					unsigned long neigh = val.first;
					edges++;
					std::shared_ptr<T> comp = std::dynamic_pointer_cast<T>(r->times(\
						std::dynamic_pointer_cast<T>(val.second)));
					std::shared_ptr<T> prev = zero;
					std::shared_ptr<T> comp_prev;
					if (prov.find(neigh) != prov.end()) {
						prev = prov[neigh];
						comp_prev = std::dynamic_pointer_cast<T>(prev->plus(comp));
					}
					else
						comp_prev = comp;
					//only process a neighbour if the residual value adds something
					if (*prev != *comp_prev) {
						std::shared_ptr<T> comp_resid;
						if (resid.find(neigh) != resid.end())
							comp_resid = std::dynamic_pointer_cast<T>(\
								resid[neigh]->plus(comp));
						else
							comp_resid = comp;
						//the new values of the neighbour
						prov[neigh] = comp_prev;
						resid[neigh] = comp_resid;
						//if the node is not in the queue, add it
						if (queue_nodes.find(neigh) == queue_nodes.end()) {
							heap_type_mohri ht_n (neigh, dec.getGraph(), str );
							q.push(ht_n);
							queue_nodes.insert(neigh);
						}
					}
				}
			}
		}
		//return the result
		std::cout << edges << " 0 0" << std::endl;
		if (prov.find(tgt) != prov.end()) {
			if (prov[tgt] != nullptr) return prov[tgt];
			else return zero;
		}
		else return zero;
	}

	/*
	 answers the query using the triangulated graph
	 with or without precomputed values
	*/
	std::shared_ptr<T> answerOnTriangulation(unsigned long src, unsigned long tgt) {
		edges = 0; hits = 0; misses = 0;
		std::unordered_map<unsigned long, std::shared_ptr<T>> prov;
		//create the path array
		std::set<unsigned long, std::function<bool(unsigned long, unsigned long)>>\
			nodes_src([&](unsigned long x, unsigned long y) {
			return dec.position(x) < dec.position(y); });
		std::set<unsigned long, std::function<bool(unsigned long, unsigned long)>>\
			nodes_tgt([&](unsigned long x, unsigned long y) {
			return dec.position(x) > dec.position(y); });
		std::queue<unsigned long> q;
		//create the forward array
		q.push(src);
		nodes_src.insert(src);
		while (!q.empty()) {
			unsigned long node = q.front();
			q.pop();
			std::unordered_set<unsigned long> fwd = dec.getForward(node);
			for (auto f : fwd) {
				if (nodes_src.find(f) == nodes_src.end()) {
					nodes_src.insert(f);
					q.push(f);
				}
			}
		}
		//create the back array
		q.push(tgt);
		nodes_tgt.insert(tgt);
		while (!q.empty()) {
			unsigned long node = q.front();
			q.pop();
			std::unordered_set<unsigned long> fwd = dec.getForward(node);;
			for (auto f : fwd) {
				if (nodes_tgt.find(f) == nodes_tgt.end()) {
					nodes_tgt.insert(f);
					q.push(f);
				}
			}
		}

		//forward sweep
		for (auto node : nodes_src) {
			std::unordered_set<unsigned long> fwd = dec.getForward(node);
			if (fwd.size() > 0) {
				for (auto f : fwd) {
					std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(node, f);
					if (ptr_edge != nullptr) {
						if (node == src) {
							prov[f] = answerProvenance(ptr_edge);
						}
						else {
							if (ptr_edge != nullptr && prov[node] != nullptr) {
								std::shared_ptr<T> sem_edge = answerProvenance(ptr_edge);
								std::shared_ptr<T> prov_times = nullptr;
								if (sem_edge != nullptr) {
									prov_times = std::dynamic_pointer_cast<T>(\
										prov[node]->times(sem_edge));
								}
								if (prov_times != nullptr && prov.find(f) != prov.end()\
									&& prov[f] != nullptr) {
									prov[f] = std::dynamic_pointer_cast<T>(
										prov[f]->plus(prov_times));
								}
								else if (prov_times != nullptr) {
									prov[f] = prov_times;
								}
							}
						}
					}
				}
			}
		}
		//add a provenance to all backward edges from the source -- IS THIS NEEDED?
		for (auto b : dec.getBackward(src)) {
			std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(src, b);
			if (ptr_edge != nullptr) {
				std::shared_ptr<T> sem_edge = answerProvenance(ptr_edge);
				if (sem_edge != nullptr) prov[b] = sem_edge;
			}
		}
		//backward sweep
		for (auto node : nodes_tgt) {
			std::unordered_set<unsigned long> back = dec.getBackward(node);
			if (node != src) {
				for (auto b : back) {
					std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(node, b);
					if (ptr_edge != nullptr && b != src) {
						if (ptr_edge != nullptr && prov[node] != nullptr) {
							std::shared_ptr<T> sem_edge = answerProvenance(ptr_edge);
							std::shared_ptr<T> prov_times = nullptr;
							if (sem_edge != nullptr) {
								prov_times = std::dynamic_pointer_cast<T>(\
									prov[node]->times(sem_edge));
							}
							if (prov_times != nullptr && prov.find(b) != prov.end()\
								&& prov[b] != nullptr) {
								prov[b] = std::dynamic_pointer_cast<T>(\
									prov[b]->plus(prov_times));
							}
							else if (prov_times != nullptr) {
								prov[b] = prov_times;
							}
						}
					}
				}
			}
		}
		//return the result
		std::cout << edges << " " << misses << " " << hits << std::endl;
		std::shared_ptr<T> zero(new T(T::zero(),k));
		if (prov.find(tgt) != prov.end()) {
			if (prov[tgt] != nullptr) return prov[tgt];
			else return zero;
		}
		else return zero;
	}

	std::shared_ptr<T> answerProvenance(std::shared_ptr<Provenance> prov) {

		std::shared_ptr<T> zero(new T(T::zero(),k));
		std::shared_ptr<T> res = nullptr;
		//computed.clear();

		if (prov != nullptr) {
			res = evaluate(prov, annotations);
		}

		return res;
	}

	void computeBag(Bag bag) {
		//precompute single-source Mohri for every node in the interface
		std::shared_ptr<T> zero(new T(T::zero(),k));
		std::shared_ptr<T> one(new T(T::one(), k));
		std::unordered_map<unsigned long, \
			std::unordered_map<unsigned long, std::shared_ptr<T>>> new_comp;
		for (auto src : bag.get_nodes()) {
			std::unordered_map<unsigned long, std::shared_ptr<T>> prov;
			std::unordered_map<unsigned long, std::shared_ptr<T>> resid;
			std::unordered_set<unsigned long> queue_nodes;
			std::unordered_set<unsigned long> bag_nodes;
			for (auto n : bag.get_nodes()) if (n != src) bag_nodes.insert(n);
			bag_nodes.insert(bag.get_node());
			prov[src] = one;
			resid[src] = one;
			std::queue<unsigned long> q;
			q.push(src);
			queue_nodes.insert(src);
			//loop through the queue
			while (!q.empty()) {
				unsigned long node = q.front(); q.pop(); queue_nodes.erase(node);
				std::shared_ptr<T> r = zero;
				if (resid.find(node) != resid.end()) r = resid[node];
				resid[node] = zero;
				//process every neighbour node
				for (auto neigh : bag_nodes) {
					if (comp.find(node) != comp.end() && \
						comp[node].find(neigh) != comp[node].end()) {
						edges++;
						std::shared_ptr<T> comp_ = std::dynamic_pointer_cast<T>(\
							r->times(std::dynamic_pointer_cast<T>(comp[node][neigh])));
						std::shared_ptr<T> prev = zero;
						std::shared_ptr<T> comp_prev;
						if (prov.find(neigh) != prov.end()) {
							prev = prov[neigh];
							comp_prev = std::dynamic_pointer_cast<T>(prev->plus(comp_));
						}
						else
							comp_prev = comp_;
						//only process a neighbour if the residual value adds something
						if (*prev != *comp_prev) {
							std::shared_ptr<T> comp_resid;
							if (resid.find(neigh) != resid.end())
								comp_resid = std::dynamic_pointer_cast<T>(\
									resid[neigh]->plus(comp_));
							else
								comp_resid = comp_;
							//the new values of the neighbour
							prov[neigh] = comp_prev;
							resid[neigh] = comp_resid;
							//if the node is not in the queue, add it
							if (queue_nodes.find(neigh) == queue_nodes.end()) {
								q.push(neigh);
								queue_nodes.insert(neigh);
							}
						}
					}
				}
			}
			//add the computations to the temporary annotations
			for (auto pair : prov) {
				unsigned long tgt = pair.first;
				if (tgt != bag.get_node() && pair.second != nullptr && *pair.second != *zero) {
					edges++;
					new_comp[src][tgt] = pair.second;
				}
			}
		}
		for (auto src : bag.get_nodes()) comp[src].insert(new_comp[src].begin(), \
			new_comp[src].end());
	}

	std::shared_ptr<T> evaluate(std::shared_ptr<Provenance> prov, \
		std::unordered_map<unsigned long, \
		std::unordered_map<unsigned long, \
		std::shared_ptr<T>>> & annotations) {
		unsigned long s = prov->source();
		unsigned long t = prov->target();
		if (prov->provenanceType() == PROVENANCE_GATE_INPUT) {
			//std::cout << "DEBUG: input (" << s << "," << t << ")" << std::endl;
			if (annotations.find(s) != annotations.end() && \
				annotations[s].find(t) != annotations[s].end()) {
				edges++;
				return annotations[s][t];
			}
			else
				return nullptr;
		}
		else {
			//check if it already has been computed
			if (computed.find(prov->getId()) == computed.end()) {
				misses++;
				std::shared_ptr<T> res = nullptr;
				std::shared_ptr<T> e_right = nullptr;
				if (prov->ptrTimesLeft() != nullptr && prov->ptrTimesRight() != nullptr) {
					std::shared_ptr<T> e_left = evaluate(prov->ptrTimesLeft(), \
						annotations);
					if (e_left != nullptr) {
						e_right = evaluate(prov->ptrTimesRight(), annotations);
					}
					if (e_left != nullptr && e_right != nullptr) {
						res = std::dynamic_pointer_cast<T>(e_left->times(e_right));
						//std::cout << "DEBUG: times (" << prov->ptrTimesLeft()->source()\
			            << "," << prov->ptrTimesLeft()->target() << ")*(" <<\
			            prov->ptrTimesRight()->source() << "," << \
			            prov->ptrTimesRight()->target() << ")" << std::endl;
					}
				}
				if (prov->ptrPlus() != nullptr) {
					std::shared_ptr<T> e_plus = evaluate(prov->ptrPlus(), annotations);
					if (res != nullptr && e_plus != nullptr)
						res = std::dynamic_pointer_cast<T>(res->plus(e_plus));
					else if (e_plus != nullptr) res = e_plus;
					//std::cout << "DEBUG: plus  +(" << s << "," << t << ")" << std::endl;
				}
				std::cout << std::flush;
				if (prov->getId() > 0) computed[prov->getId()] = res;
				return res;
			}
			else { //already computed
				hits++;
				return computed[prov->getId()];
			}
		}
	}

};


#endif /* SemiringQueryProcessor_h */
//
//  SemiringQueryProcessor.h
//  graph-provenance
//
//  Created by Silviu Maniu on 30/08/16.
