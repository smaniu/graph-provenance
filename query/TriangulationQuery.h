//
//  TriangulationQuery.h
//  graph-provenance
//
//  Created by Silviu Maniu on 18/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef TriangulationQuery_h
#define TriangulationQuery_h

#include <memory>
#include <queue>
#include <set>

#include "../structure/TreeDecomposition.h"
#include "../structure/Provenance.h"
#include "../semiring/Semiring.h"

class TriangulationQuery {
private:
  std::shared_ptr<Provenance> ptr_prov;
  
public:
  //computing on the tree 
  void preparePairQuery(TreeDecomposition& dec, unsigned long src,\
                        unsigned long tgt){
    std::unordered_map<unsigned long, std::shared_ptr<Provenance>> prov;
    std::unordered_set<unsigned long> back_nodes;
    unsigned long cid = dec.getNumberCircuits();
    //create the path array
    std::set<unsigned long, std::function<bool (unsigned long, unsigned long)>>\
            nodes_src([&](unsigned long x, unsigned long y){
              return dec.position(x)<dec.position(y);});
    std::set<unsigned long, std::function<bool (unsigned long, unsigned long)>>\
            nodes_tgt([&](unsigned long x, unsigned long y){
              return dec.position(x)>dec.position(y);});
    std::queue<unsigned long> q;
    //create the forward array
    q.push(src);
    nodes_src.insert(src);
    while(!q.empty()){
      unsigned long node = q.front();
      q.pop();
      std::unordered_set<unsigned long> fwd = dec.getForward(node);
      for(auto f:fwd){
        if(nodes_src.find(f)==nodes_src.end()){
          nodes_src.insert(f);
          q.push(f);
        }
      }
    }
    //create the back array
    q.push(tgt);
    nodes_tgt.insert(tgt);
    while(!q.empty()){
      unsigned long node = q.front();
      q.pop();
      std::unordered_set<unsigned long> fwd = dec.getForward(node);;
      for(auto f:fwd){
        if(nodes_tgt.find(f)==nodes_tgt.end()){
          nodes_tgt.insert(f);
          q.push(f);
        }
      }
    }
    
    //forward sweep
    for(auto node:nodes_src){
      std::unordered_set<unsigned long> fwd = dec.getForward(node);
      if(fwd.size()>0){
        for(auto f:fwd){//create provenance for each node in the fwd queue
          std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(node, f);
          if(ptr_edge!=nullptr){
            if(node==src){
              prov[f] = ptr_edge;
            }
            else{
              cid++;
              std::shared_ptr<Provenance>\
              prov_circ(new Provenance(src, f, PROVENANCE_GATE_INTER, cid));
              prov_circ->set_times_left(prov[node]);
              prov_circ->set_times_right(ptr_edge);
              if(prov.find(f)!=prov.end())
                prov_circ->set_plus(prov[f]);
              prov[f] = prov_circ;
            }
          }
        }
      }
    }
    //add a provenance to all backward edges from the source -- IS THIS NEEDED?
    for(auto b: dec.getBackward(src)){
      std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(src, b);
      if(ptr_edge!=nullptr) prov[b]=ptr_edge;
    }
    //backward sweep
    for(auto node:nodes_tgt){
      std::unordered_set<unsigned long> back = dec.getBackward(node);
      if(node!=src){
        for(auto b:back){
          std::shared_ptr<Provenance> ptr_edge = dec.getEdgeProvenance(node, b);
          if(ptr_edge!=nullptr&&b!=src){
            cid++;
            std::shared_ptr<Provenance>\
              prov_circ(new Provenance(src, b, PROVENANCE_GATE_INTER, cid));
            prov_circ->set_times_left(prov[node]);
            prov_circ->set_times_right(ptr_edge);
            if(prov.find(b)!=prov.end())
              prov_circ->set_plus(prov[b]);
            prov[b] = prov_circ;
          }
        }
      }
    }
    //put the result in the provenance pointer
    if(prov.find(tgt)!=prov.end()) ptr_prov=prov[tgt];
    else ptr_prov=nullptr;
  }
  
  std::shared_ptr<Provenance> provPtr() const {return ptr_prov;}
  
};


#endif /* TriangulationQuery_h */
