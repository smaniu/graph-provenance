//
//  PrintingSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 29/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef PrintingSemiring_h
#define PrintingSemiring_h

#include <string>
#include <unordered_set>

#include "Semiring.h"

struct PrintingContainer{
  std::unordered_set<std::string> elements;
};

std::ostream& operator<<(std::ostream& out, const PrintingContainer& cont){
  out << cont.elements.size() << std::endl;
  for(std::string val:cont.elements) out << val << std::endl;
  return out;
}

std::istream& operator>>(std::istream& is, PrintingContainer& cont){
  unsigned long number;
  is >> number;
  for(unsigned long i=0;i<number;i++){
    std::string elem;
    is >> elem;
    cont.elements.insert(elem);
  }
  return is;
}

bool operator==(const PrintingContainer& lhs, const PrintingContainer& rhs){
  return rhs.elements == lhs.elements;
}

bool operator!=(const PrintingContainer& lhs, const PrintingContainer& rhs){
  return rhs.elements != lhs.elements;
}

bool operator<(const PrintingContainer& lhs, const PrintingContainer& rhs){
  return lhs.elements.size() < rhs.elements.size();
}

class PrintingSemiring : public Semiring<PrintingContainer>{
public:
  PrintingSemiring() : Semiring(PrintingSemiring::zero()) {}
  
  PrintingSemiring(PrintingContainer val, int k=0) : Semiring(val,k) {};
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<PrintingSemiring> r =\
      std::dynamic_pointer_cast<PrintingSemiring>(rval);
    PrintingContainer new_value;
    new_value.elements.insert(value.elements.begin(),\
                              value.elements.end());
    new_value.elements.insert(r->value.elements.begin(),\
                              r->value.elements.end());
    std::shared_ptr<Semiring> rv(new PrintingSemiring(new_value));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<PrintingSemiring> r =
      std::dynamic_pointer_cast<PrintingSemiring>(rval);
    PrintingContainer new_value;
    for(auto lval: value.elements)
      for(auto rval: r->value.elements)
        new_value.elements.insert(lval+" "+rval);
    std::shared_ptr<Semiring> rv(new PrintingSemiring(new_value));
    return rv;
  }

  virtual std::shared_ptr<Semiring> star()
  override {
    PrintingContainer new_value;
    for(auto val: value.elements)
      new_value.elements.insert("("+val+")*");
    std::shared_ptr<Semiring> rv(new PrintingSemiring(new_value));
    return rv;
  }

  // properties
  // TODO
  virtual bool isCommutative() override {
	  return false;
  }
  virtual bool isIdempotent() override {
	  return false;
  }
  virtual bool isMultIdempotent() override {
	  return false;
  }
  virtual bool isKClos() override {
	  return false;
  }
  virtual bool isNaturallyOrdered() override {
	  return false;
  }
  virtual bool isTotallyOrdered() override {
	  return false;
  }
  virtual bool isAbsorptive() override {
	  return false;
  }
  virtual bool isAStarSemiring() override {
	  return false;
  }
  virtual long unsigned int getK() override {
	  return k;
  }
  
  static PrintingContainer zero() {
    PrintingContainer empty;
    return empty;
  }
  
  static PrintingContainer one() {
    PrintingContainer empty;
    return empty;
  }
  
  static std::string get_label() {return "id";}
};

#endif /* PrintingSemiring_h */
