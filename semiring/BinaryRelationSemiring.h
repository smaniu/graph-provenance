//
//  AlgebraSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 01/09/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef BinaryRelationSemiring_h
#define BinaryRelationSemiring_h

#include "Semiring.h"

#include <unordered_set>
#include <utility>

struct TupleContainer{
  std::unordered_set<std::pair<unsigned long, unsigned long>> elements;
};

std::ostream& operator<<(std::ostream& out, const TupleContainer& cont){
  out << cont.elements.size() << std::endl;
  for(auto val:cont.elements) out << val.first << " " <<val.second << std::endl;
  return out;
}

std::istream& operator>>(std::istream& is, TupleContainer& cont){
  unsigned long number;
  is >> number;
  for(unsigned long i=0;i<number;i++){
    unsigned long first;
    unsigned long second;
    is >> first >> second;
    std::pair<unsigned long, unsigned long> elem = std::make_pair(first, second);
    cont.elements.insert(elem);
  }
  return is;
}

bool operator==(const TupleContainer& lhs, const TupleContainer& rhs){
  return rhs.elements == lhs.elements;
}

bool operator!=(const TupleContainer& lhs, const TupleContainer& rhs){
  return rhs.elements != lhs.elements;
}

bool operator<(const TupleContainer& lhs, const TupleContainer& rhs){
  return false;
}

class BinaryRelationSemiring : public Semiring<TupleContainer>{
public:
  BinaryRelationSemiring() : Semiring(BinaryRelationSemiring::zero()) {}
  
  BinaryRelationSemiring(TupleContainer val, int k=0) : Semiring(val,k) {};
  
  BinaryRelationSemiring(const BinaryRelationSemiring& other) :
    BinaryRelationSemiring(other.value) {};
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<BinaryRelationSemiring> r =\
    std::dynamic_pointer_cast<BinaryRelationSemiring>(rval);
    TupleContainer new_value;
    new_value.elements.insert(value.elements.begin(),\
                              value.elements.end());
    new_value.elements.insert(r->value.elements.begin(),\
                              r->value.elements.end());
    std::shared_ptr<Semiring> rv(new BinaryRelationSemiring(new_value));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<BinaryRelationSemiring> r =
        std::dynamic_pointer_cast<BinaryRelationSemiring>(rval);
    TupleContainer new_value;
    if(this->value==one()) {
      std::shared_ptr<Semiring> rv(new BinaryRelationSemiring(r->value));
      return rv;
    }
    else if(r->value==one()) {
      std::shared_ptr<Semiring> rv(new BinaryRelationSemiring(this->value));
      return rv;
    }
    else {
      for(auto lval: value.elements) //nested loop join - TO REVISE
        for(auto rval: r->value.elements) {
          if(lval.second==rval.first){
            std::pair<unsigned long, unsigned long> elem =\
              std::make_pair(lval.first, rval.second);
            new_value.elements.insert(elem);
          }
          if(lval == std::make_pair(0UL,0UL)) new_value.elements.insert(rval);
	  if(rval == std::make_pair(0UL,0UL)) new_value.elements.insert(lval);
        }
      std::shared_ptr<Semiring> rv(new BinaryRelationSemiring(new_value));
      return rv;
    }
  }

  virtual std::shared_ptr<Semiring> star()\
  override {
    std::shared_ptr<Semiring> rv(new BinaryRelationSemiring(BinaryRelationSemiring::one()));
    int i = 0;
    while(i < 10) {
      std::shared_ptr<Semiring> rv2 = \
          rv->times(std::shared_ptr<Semiring>(new BinaryRelationSemiring(value)));
      std::shared_ptr<Semiring> one(new BinaryRelationSemiring(BinaryRelationSemiring::one()));
      rv2 = rv2->plus(one);

      if (*rv2 == *rv) break;
      rv = rv2;
      i++;
    } 
    return rv;
  }

  // properties
  virtual bool isCommutative() override {
	  return false;
  }
  virtual bool isIdempotent() override {
	  return true; // TODO
  }
  virtual bool isMultIdempotent() override {
	  return false;
  }
  virtual bool isKClos() override {
	  return true;
  }
  virtual bool isNaturallyOrdered() override {
	  return true; // TODO
  }
  virtual bool isTotallyOrdered() override {
	  return false; // TODO
  }
  virtual bool isAbsorptive() override {
	  return false; // TODO
  }
  virtual bool isAStarSemiring() override {
	  return true;
  }
  virtual long unsigned int getK() override {
	  return k; // TODO
  }
  
  static TupleContainer zero() {
    TupleContainer empty;
    return empty;
  }
  
  static TupleContainer one() {
    TupleContainer one;
    std::pair<unsigned long, unsigned long> elem = std::make_pair(0, 0);
    one.elements.insert(elem);
    return one;
  }
  
  static std::string get_label() {return "tuples";}
};

#endif /* BinaryRelationSemiring_h */
