//
//  Semiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 13/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef Semiring_h
#define Semiring_h

#include <typeinfo>
#include <iostream>
#include <memory>
#include <cstring>

template<typename C> class Semiring;
template<typename C> std::ostream& operator<<(std::ostream&, const Semiring<C>& semi);
template<typename C> bool operator==(const Semiring<C>&, const Semiring<C>& semi);
template<typename C> bool operator!=(const Semiring<C>&, const Semiring<C>& semi);
template<typename C> bool operator<(const Semiring<C>&, const Semiring<C>& semi);

template<typename C> class Semiring{
protected:
  C value;
  long unsigned int k=10; //for TopKSemiring
public:
  typedef C value_type;
  
  Semiring(C val, int k=10) : value(val), k(k) {};
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval) = 0;
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval) = 0;

  virtual std::shared_ptr<Semiring> star() = 0;

  C getValue() const { return value; };

  void setK(int K) { k = K; };

  // properties
  virtual bool isCommutative() = 0;
  virtual bool isIdempotent() = 0;
  virtual bool isMultIdempotent() = 0;
  virtual bool isKClos() = 0;
  virtual bool isNaturallyOrdered() = 0;
  virtual bool isTotallyOrdered() = 0;
  virtual bool isAbsorptive() = 0;
  virtual bool isAStarSemiring() = 0;
  virtual long unsigned int getK() = 0; // needs to throw execption if non-applicable?

  friend std::ostream& operator<<<>(std::ostream& out, const Semiring<C>& semi);

  friend bool operator==<>(const Semiring& lhs, const Semiring& rhs);

  friend bool operator!=<>(const Semiring& lhs, const Semiring& rhs);

  friend bool operator< <>(const Semiring& lhs, const Semiring& rhs);

  // Part of DistributiveLattice interface but needs to be known for each semiring
  virtual std::shared_ptr<Semiring<C>> getSimpleValue(std::shared_ptr<Semiring<C>>) {
	  std::shared_ptr<Semiring> rv(NULL);
	  return rv;
  }

  virtual bool lessThan(const Semiring &rhs) const {
    return rhs.value < this->value;
  }
};

template<typename C> std::ostream& operator<<(std::ostream& out, const Semiring<C>& semi) {
  out << semi.value;
  return out;
}

template<typename C> bool operator==(const Semiring<C>& lhs, const Semiring<C>& rhs) {
  return rhs.value == lhs.value;
}

template<typename C> bool operator!=(const Semiring<C>& lhs, const Semiring<C>& rhs) {
  return rhs.value != lhs.value;
}

template<typename C> bool operator<(const Semiring<C>& lhs, const Semiring<C>& rhs) {
  return lhs.lessThan(rhs);
}

#endif /* Semiring_h */
