//
//  TropicalSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 29/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef TropicalSemiring_h
#define TropicalSemiring_h

#include "Semiring.h"

#include <limits>
#include <algorithm>

class TropicalSemiring : public Semiring<double>{
public:
  TropicalSemiring(): Semiring(TropicalSemiring::zero()) {};
  
  TropicalSemiring(double val, int k=0) : Semiring(val,k) {};
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<TropicalSemiring> r =\
      std::dynamic_pointer_cast<TropicalSemiring>(rval);
    std::shared_ptr<Semiring> rv(new TropicalSemiring(std::min(value,r->value)));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<TropicalSemiring> r =\
    std::dynamic_pointer_cast<TropicalSemiring>(rval);
    std::shared_ptr<Semiring> rv(new TropicalSemiring(value+r->value));
    return rv;
  }

  virtual std::shared_ptr<Semiring> star()\
  override {
    if(value == 0) {
      std::shared_ptr<Semiring> rv(new TropicalSemiring(TropicalSemiring::zero()));
      return rv;
    } else {
      std::shared_ptr<Semiring> rv(new TropicalSemiring(TropicalSemiring::one()));
      return rv;
    }
  }

  // properties
  virtual bool isCommutative() override {
  	return true;
  }
  virtual bool isIdempotent() override {
  	return true;
  }
  virtual bool isMultIdempotent() override {
  	return false;
  }
  virtual bool isKClos() override {
  	return true; 
  }
  virtual bool isNaturallyOrdered() override {
  	return true;
  }
  virtual bool isTotallyOrdered() override {
  	return true;
  }
  virtual bool isAbsorptive() override {
  	return true; 
  }
  virtual bool isAStarSemiring() override {
  	return true;
  }
  virtual long unsigned int getK() override {
  	return k;
  }
  
  static double zero() {
    return std::numeric_limits<double>::max();
  }
  
  static double one() {return 0;}
  
  static std::string get_label() {return "tropical";}
};

#endif /* TropicalSemiring_h */
