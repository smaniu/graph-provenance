//
//  TopKSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 28/09/2016.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef TopKSemiring_h
#define TopKSemiring_h

#include <vector>
#include <algorithm>
#include <utility>
#include <string>
#include <iostream>

#include "Semiring.h"

//holds a distance-path pair
struct DistPathPair{
  std::pair<double, std::vector<std::string>> el;
};

std::ostream& operator<<(std::ostream& out, const DistPathPair& dpath){
  out << dpath.el.first << std::endl << dpath.el.second.size() << std::endl;
  for(auto edg:dpath.el.second) out << edg << std::endl;
  return out;
}

std::istream& operator>>(std::istream& is, DistPathPair& dpath){
  double dist;
  unsigned long num;
  std::vector<std::string> path;
  is >> dist;
  is >> num;
  for(unsigned long i=0;i<num;i++){
    std::string edg;
    is >> edg;
    path.push_back(edg);
  }
  dpath.el = std::make_pair(dist,path);
  return is;
}

bool operator<(const DistPathPair& lhs, const DistPathPair& rhs){
  return lhs.el.first<rhs.el.first;
}

bool operator==(const DistPathPair& lhs, const DistPathPair& rhs){
  return (rhs.el.first == lhs.el.first) && (rhs.el.second == rhs.el.second);
}

bool operator!=(const DistPathPair& lhs, const DistPathPair& rhs){
  return (rhs.el.first != lhs.el.first) || (rhs.el.second != rhs.el.second);
}

//holds the top-k paths by dsitance
struct TopKContainer{
  std::vector<DistPathPair> elements;
};

std::ostream& operator<<(std::ostream& out, const TopKContainer& cont){
  out << cont.elements.size() << std::endl;
  for(auto val:cont.elements) out << val << std::endl;
  return out;
}

std::istream& operator>>(std::istream& is, TopKContainer& cont){
  unsigned long number;
  is >> number;
  for(unsigned long i=0;i<number;i++){
    DistPathPair elem;
    is >> elem;
    cont.elements.push_back(elem);
  }
  return is;
}

/* We add this because of the heap_type defined in 'SemiringQueryProcessor.h' .
 * 
 * We do not use this because we cannot apply Dijkstra for a k-closed semiring 
 * but we need to define the operator < for the Semiring and the underlying container
 * to be able to compile.
 *
 * In order to implement it in a consistent way, we always return 'false', meaning 
 * the struct heap_type will compare based on the node ordering 
 * */
bool operator<(const TopKContainer& , const TopKContainer& ){
  return false; 
}

bool operator==(const TopKContainer& lhs, const TopKContainer& rhs){
  return rhs.elements == lhs.elements;
}

bool operator!=(const TopKContainer& lhs, const TopKContainer& rhs){
  return rhs.elements != lhs.elements;
}

//Semiring for computing top-k paths
class TopKSemiring: public Semiring<TopKContainer>{
public:

	TopKSemiring() : Semiring(zero()) {}

	TopKSemiring(TopKContainer val, int k) : Semiring(val,k) {}

  
	virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
	override {
		std::shared_ptr<TopKSemiring> r = std::dynamic_pointer_cast<TopKSemiring>(rval);
		r->setK(k);
		TopKContainer new_value;
		new_value.elements.insert(new_value.elements.begin(),\
								value.elements.begin(), value.elements.end());
		new_value.elements.insert(new_value.elements.begin(),\
                              r->value.elements.begin(),r->value.elements.end());
		std::sort(new_value.elements.begin(), new_value.elements.end());
		if(k<new_value.elements.size()) new_value.elements.resize(k);
		std::shared_ptr<Semiring> rv(new TopKSemiring(new_value,k));
		return rv;
	}
  
	virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
	override {
	    std::shared_ptr<TopKSemiring> r = std::dynamic_pointer_cast<TopKSemiring>(rval);
		r->setK(k);
		TopKContainer new_value;
		for(auto lval: value.elements)
		for(auto rval: r->value.elements){
			DistPathPair dpath;
			std::vector<std::string> path(lval.el.second);
			path.insert(path.end(), rval.el.second.begin(), rval.el.second.end());
			dpath.el = std::make_pair(lval.el.first+rval.el.first, path);
			new_value.elements.push_back(dpath);
		}
		std::sort(new_value.elements.begin(), new_value.elements.end());
		if(k<new_value.elements.size()) new_value.elements.resize(k);
		std::shared_ptr<Semiring> rv(new TopKSemiring(new_value,k));
		return rv;
	}

  //!\\ Not correctly implemented
  virtual std::shared_ptr<Semiring> star()\
  override {
    std::shared_ptr<Semiring> rv(new TopKSemiring(TopKSemiring::one(),k));
    for(long unsigned int i = 1; i < k; i++) {
      rv = rv->times(std::shared_ptr<Semiring>(new TopKSemiring(value,k)));
      std::shared_ptr<Semiring> one(new TopKSemiring(TopKSemiring::one(),k));
      rv = rv->plus(one); 
    } 
    return rv;
  }

  // properties
  virtual bool isCommutative() override {
	  return true;
  }
  virtual bool isIdempotent() override {
	  return false;
  }
  virtual bool isMultIdempotent() override {
	  return false;
  }
  virtual bool isKClos() override {
	  return true;
  }
  virtual bool isNaturallyOrdered() override {
	  return true; // TODO
  }
  virtual bool isTotallyOrdered() override {
	  return false;
  }
  virtual bool isAbsorptive() override {
	  return false; // TODO
  }
  virtual bool isAStarSemiring() override {
	  return true;
  }
  virtual long unsigned int getK() override {
	  return k;
  }
  
  static TopKContainer zero() {
    TopKContainer empty;
    return empty;
  }
  
  static TopKContainer one() {
    DistPathPair z;
    std::vector<std::string> zero_vector;
    z.el = std::make_pair(0,zero_vector);
    TopKContainer empty;
    empty.elements.push_back(z);
    return empty;
  }
  
  static std::string get_label() {return "knn";}
};

#endif /* TopKSemiring_h */
