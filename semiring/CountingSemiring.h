//
//  CountingSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 14/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef CountingSemiring_h
#define CountingSemiring_h

#include <string.h>

#include "Semiring.h"

class CountingSemiring : public Semiring<unsigned long>{
public:
  CountingSemiring() : Semiring(0) {}
  
  CountingSemiring(unsigned long val, int k=0) : Semiring(val,k) {
  };
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<CountingSemiring> r =\
      std::dynamic_pointer_cast<CountingSemiring>(rval);
    std::shared_ptr<Semiring> rv(new CountingSemiring(value+r->value));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<CountingSemiring> r =\
      std::dynamic_pointer_cast<CountingSemiring>(rval);
    std::shared_ptr<Semiring> rv(new CountingSemiring(value*r->value));
    return rv;
  }

  virtual std::shared_ptr<Semiring> star()\
  override {
    if(value!=0) { std::shared_ptr<Semiring> rv(new CountingSemiring(std::numeric_limits<long unsigned int>::infinity())); return rv; }
    else { std::shared_ptr<Semiring> rv(new CountingSemiring(1));
    return rv; } 
  }

  // properties
  virtual bool isCommutative() override {
	  return true;
  }
  virtual bool isIdempotent() override {
	  return false;
  }
  virtual bool isMultIdempotent() override {
	  return false;
  }
  virtual bool isKClos() override {
	  return false;
  }
  virtual bool isNaturallyOrdered() override {
	  return false; // TODO
  }
  virtual bool isTotallyOrdered() override {
	  return false; // TODO
  }
  virtual bool isAbsorptive() override {
	  return false;
  }
  virtual bool isAStarSemiring() override {
	  return false;
  }
  virtual long unsigned int getK() override {
	  return k; // TODO
  }
  
  static unsigned long zero() {return 0;}
  
  static unsigned long one() {return 1;}
  
  static std::string get_label() {return "count";}
};

#endif /* CountingSemiring_h */
