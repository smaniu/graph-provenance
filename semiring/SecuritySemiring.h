//
//  SecuritySemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 30/08/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef SecuritySemiring_h
#define SecuritySemiring_h

#include "Semiring.h"
#include "DistributiveLattice.h"

#include <limits>
#include <algorithm>

class SecuritySemiring : public DistributiveLattice<unsigned long>{
public:
  SecuritySemiring() : DistributiveLattice<unsigned long>(0uL) {
    hasDecomposition = false;
  };
  
  SecuritySemiring(unsigned long val, int k=0, bool performDecomposition = true) : DistributiveLattice<unsigned long>(val,k) {
    if(performDecomposition) decompose();
    else hasDecomposition = false;
  };
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<SecuritySemiring> r =\
      std::dynamic_pointer_cast<SecuritySemiring>(rval);
    std::shared_ptr<Semiring> rv(new SecuritySemiring(std::max(value,r->value),0,false));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<SecuritySemiring> r =\
      std::dynamic_pointer_cast<SecuritySemiring>(rval);
    std::shared_ptr<Semiring> rv(new SecuritySemiring(std::min(value,r->value),0,false));
    return rv;
  }

  //!\\ Not correctly implemented
  virtual std::shared_ptr<Semiring> star()\
  override {
    std::shared_ptr<Semiring> rv(new SecuritySemiring(SecuritySemiring::one()));
    return rv;
  }

  // properties
  virtual bool isTotallyOrdered() override {
	  return true;
  }

  static unsigned long zero() {return 0;}
  
  static unsigned long one() {return std::numeric_limits<unsigned long>::max();}
  
  static std::string get_label() {return "security";}

  // DistributiveLattice interface
  virtual void decompose() override {
    std::shared_ptr<Semiring> same(new SecuritySemiring(this->value,0,false));
    chain_decomposition.push_back(same);
    //TODO add to simpleElts if dimension not seen before
  }

  //!\\ Not correctly implemented
  virtual std::shared_ptr<Semiring> getSimpleValue(std::shared_ptr<Semiring>)\
  override {
    std::shared_ptr<Semiring> rv(new SecuritySemiring(SecuritySemiring::one()));
    return rv;
  }
};

#endif /* SecuritySemiring_h */
