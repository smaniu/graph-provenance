//
// DistributiveLattice.h
// graph-provenance
//
// Created by Yann Ramusat on 06/12/19.
// 

#ifndef DistributiveLattice_h
#define DistributiveLattice_h

#include "Semiring.h"
#include <vector>

template<typename C> class DistributiveLattice: public Semiring<C>{
protected:
  std::vector<std::shared_ptr<Semiring<C>>> chain_decomposition;
  bool hasDecomposition = false;

public:
  static std::vector<std::shared_ptr<Semiring<C>>> simpleElts;

  DistributiveLattice(C val, long unsigned int k = 0) : Semiring<C>(val,k) {
  }

  // properties
  virtual bool isCommutative() override {
	  return true;
  }
  virtual bool isIdempotent() override {
	  return true;
  }
  virtual bool isMultIdempotent() override {
	  return true;
  }
  virtual bool isKClos() override {
	  return true;
  }
  virtual bool isNaturallyOrdered() override {
	  return true;
  }
  // virtual bool isTotallyOrdered() 
  virtual bool isAbsorptive() override {
	  return true;
  }
  virtual bool isAStarSemiring() override {
	  return true;
  }
  virtual long unsigned int getK() override {
	  return this->k;
  }

  // DistributiveLattice interface
  virtual void decompose() = 0;

  virtual bool isDecomposed() {
	  return hasDecomposition;
  }

  /*
   * Return true if either (value v rvalue = value) or (value v rvalue = rvalue) 
   * Means value<=rvalue or rvalue<=value for the natural order
   */ 
  virtual bool isComparableWith(std::shared_ptr<Semiring<C>> rvalue) {
	  std::shared_ptr<Semiring<C>> somme(this->plus(rvalue));
	  if((*this == *somme) || (*somme == *rvalue)) return true;
	  else return false;
  }

  /* Ajoutée à Semiring.h car doit être connue pour tous les semi-anneaux pour pouvoir compiler Dijkstra 
   * virtual std::shared_ptr<Semiring<C>> getSimpleValue(std::shared_ptr<Semiring<C>>) = 0;
   */
};

template<typename C> 
std::vector<std::shared_ptr<Semiring<C>>> DistributiveLattice<C>::simpleElts;

#endif /* DistributiveLattice_h */
