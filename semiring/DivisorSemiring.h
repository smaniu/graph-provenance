//
// DivisorSemiring.h
// graph-provenance
//
// Created by Yann Ramusat on 10/12/19
//

#ifndef DivisorSemiring_h
#define DivisorSemiring_h

#include "Semiring.h"
#include "DistributiveLattice.h"

#include <limits>
#include <algorithm>
#include <cmath>

#define MAX_VALUE 600uL

class DivisorSemiring: public DistributiveLattice<unsigned long>{
public:
  DivisorSemiring() : DistributiveLattice<unsigned long>(DivisorSemiring::zero()) {
    hasDecomposition = false;
  };

  DivisorSemiring(unsigned long val, int k=0, bool performDecomposition = true) : DistributiveLattice<unsigned long>(val,k) {
    if(performDecomposition) {
	    // because we never want 'one' to be decomposed
	    if(val == DivisorSemiring::one()) hasDecomposition = false;
	    else decompose();
    }
    else hasDecomposition = false;
  };

  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    unsigned long a = this->value;
    unsigned long b = rval->getValue();
    while(a != b) {
      if (a>b) a = a-b;
      else b = b-a;
    }
    std::shared_ptr<Semiring> rv(new DivisorSemiring(a,0,false));
    return rv;
  }

  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    unsigned long a = this->value;
    unsigned long b = rval->getValue();
    std::shared_ptr<Semiring> gcd_ptr = this->times(rval);
    unsigned long gcd = gcd_ptr->getValue();
    std::shared_ptr<Semiring> rv(new DivisorSemiring(a*b/gcd,0,false));
    return rv;
  }

  //!\\ Not correctly implemented
  virtual std::shared_ptr<Semiring> star()\
  override {
    std::shared_ptr<Semiring> rv(new DivisorSemiring(DivisorSemiring::one()));
    return rv;
  }

  // properties
  virtual bool isTotallyOrdered() override {
	  return false;
  }
  
  static unsigned long zero() {return 1ul;}

  static unsigned long one() {return MAX_VALUE;}

  static std::string get_label() {return "divisor";}

  // DistributiveLattice interface

  // this function is optimized for and works only with integer powers of 2, 3, 5, 7, 11 and 13.
  virtual void decompose() override {
    unsigned long val = this->getValue();
    //std::cout << "decompose: getValue()  " << val << std::endl;
    if (val == 1uL) return;
    unsigned long prime[6] = {2uL, 3uL, 5uL, 7uL, 11uL, 13uL};
    int i = 0;
    int j = 0;
begin:
    while(!(val%prime[i]) ) {
      j++;
      val /= prime[i];
    }
    std::shared_ptr<Semiring> power(new DivisorSemiring(pow(prime[i],j),0,false));
    //std::cout << "simple value obtained: " <<  *power << std::endl;
    j = 0;
    i++;
    if(power->getValue() == 1uL) goto begin; // skip 1 values resulting in previous prime numbers not being in the dec
    chain_decomposition.push_back(power);
    bool isNew = true;
    // add to simpleElts if dimension not seen before
    //std::cout << "inspecting simpleElts" << std::endl;
    for(const auto& value: this->simpleElts) {
      std::shared_ptr<DistributiveLattice> dlvalue = std::dynamic_pointer_cast<DistributiveLattice>(value);
      //std::cout << "one simple value in the graph is: " << *dlvalue << std::endl;
      if(dlvalue->isComparableWith(power)) {
	      isNew = false;
	      break;
      }
    }
    if(isNew) {
	    //std::cout << "add " << *power << " to simpleElts" << std::endl;
	    this->simpleElts.push_back(power);
	    //std::cout << "done." << std::endl;
    }
    // end of code
    if(val!=1uL) goto begin;
    return;
  }

  virtual std::shared_ptr<Semiring> getSimpleValue(std::shared_ptr<Semiring> dim)\
  override {
    // enforce dim to be a simple value?
    std::shared_ptr<Semiring> zero(new DivisorSemiring(DivisorSemiring::zero()));
    //std::cout << "getSimpleValue from " << *this << " using dim= " << *dim << std::endl;
    for(const auto& value: this->chain_decomposition) {
    	//std::cout << "obtained in decomposition: " << *value << std::endl;
	if(value->getValue() == DivisorSemiring::zero()) {
		std::cout << "we skip value 1 in decomposition - why does it appear?" << std::endl;
		continue;
	};
	std::shared_ptr<DistributiveLattice> dlvalue = std::dynamic_pointer_cast<DistributiveLattice>(value);
	//std::cout << "isCompWithDim?: " << dlvalue->isComparableWith(dim) << std::endl;
	if(dlvalue->isComparableWith(dim)) return value;
    }
    return zero;
  }

  virtual bool lessThan(const Semiring &rhs) const override {
    return rhs.getValue() > this->value;
  }
};

#endif /* DivisorSemiring_h */
