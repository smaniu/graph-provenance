//
//  FormulaSemiring.h
//  graph-provenance
//
//  Created by Silviu Maniu on 01/09/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef FormulaSemiring_h
#define FormulaSemiring_h

#include <string>

#include "Semiring.h"

class FormulaSemiring : public Semiring<std::string>{
public:
  FormulaSemiring() : Semiring(FormulaSemiring::zero()) {}
  
  FormulaSemiring(std::string val, int k=0) : Semiring(val,k) {};
  
  virtual std::shared_ptr<Semiring> plus(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<FormulaSemiring> r =\
    std::dynamic_pointer_cast<FormulaSemiring>(rval);
    std::shared_ptr<Semiring> rv(new FormulaSemiring("("+value+"∨"+r->value+")"));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> times(std::shared_ptr<Semiring> rval)\
  override {
    std::shared_ptr<FormulaSemiring> r =
    std::dynamic_pointer_cast<FormulaSemiring>(rval);
    std::shared_ptr<Semiring> rv(new FormulaSemiring("("+value+"∧"+r->value+")"));
    return rv;
  }
  
  virtual std::shared_ptr<Semiring> star()\
  override {
    std::shared_ptr<Semiring> rv(new FormulaSemiring("("+value+")*"));
    return rv;
  }

  // properties
  // TODO
  virtual bool isCommutative() override {
	  return true;
  }
  virtual bool isIdempotent() override {
	  return true;
  }
  virtual bool isMultIdempotent() override {
	  return true;
  }
  virtual bool isKClos() override {
	  return true;
  }
  virtual bool isNaturallyOrdered() override {
	  return false;
  }
  virtual bool isTotallyOrdered() override {
	  return false;
  }
  virtual bool isAbsorptive() override {
	  return true;
  }
  virtual bool isAStarSemiring() override {
	  return true;
  }
  virtual long unsigned int getK() override {
	  return k;
  }

  
  static std::string zero() {
    return std::string("");
  }
  
  static std::string one() {
    return std::string("");
  }
  
  static std::string get_label() {return "formula";}
};

#endif /* FormulaSemiring_h */
