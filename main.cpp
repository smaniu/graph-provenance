#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <string>
#include <limits>
#include <chrono>

#include <boost/program_options.hpp>

//common stuff, data structures
#include "common.h"
#include "structure/Graph.h"
#include "structure/TreeDecomposition.h"

//decomposition strategies
#include "strategy/PermutationStrategy.h"
#include "strategy/DegreePermutationStrategy.h"
#include "strategy/FillInPermutationStrategy.h"
#include "strategy/DegreeFillInPermutationStrategy.h"
#include "strategy/IdPermutationStrategy.h"

//queries
#include "query/TriangulationQuery.h"
#include "query/SemiringQueryProcessor.h"

//semirings
#include "semiring/Semiring.h"
#include "semiring/CountingSemiring.h"
#include "semiring/TropicalSemiring.h"
#include "semiring/PrintingSemiring.h"
#include "semiring/FormulaSemiring.h"
#include "semiring/TopKSemiring.h"
#include "semiring/BinaryRelationSemiring.h"
#include "semiring/DistributiveLattice.h"
#include "semiring/SecuritySemiring.h"
#include "semiring/DivisorSemiring.h"



namespace po = boost::program_options;

std::unordered_map<std::string, int> algo_map{ {"Mohri",QUERY_ALGORITHM_MOHRI},
	{"MinPathTriangulation",QUERY_ALGORITHM_MINPATH}, {"NodeElimination",QUERY_ALGORITHM_NODELIM},
	{"Dijkstra",QUERY_ALGORITHM_DIJKSTRA}, {"PrecomputedProvenance",QUERY_ALGORITHM_PRECOMP}, {"ExtendedDijkstra",QUERY_ALGORITHM_EXT_DIJKSTRA},
	{"Access",QUERY_ALGORITHM_ACCESS} };



template<class S> void queryOnSemiring(TreeDecomposition& decomposition,
                                       std::string file_name_graph, int k,
                    std::string met, int s, int t, PermutationStrategy& str) {
	//evaluating query
	std::cerr << "preparing annotations..." << std::flush;
	auto t0 = std::chrono::system_clock::now();
	SemiringQueryProcessor<S> qryProc(decomposition, file_name_graph, str, k, true);
	qryProc.init();
	auto t1 = std::chrono::system_clock::now();
	auto diff = t1 - t0;
	std::cerr << (float)diff.count() * std::chrono::system_clock::period::num / std::chrono::system_clock::period::den << std::endl;
	t0 = std::chrono::system_clock::now();
	//clock_t cpu_t = clock();
	std::cerr << "Algorithm - " << met << std::endl;
	std::shared_ptr<S> ans = qryProc.answer(s, t, algo_map[met]);
	t1 = std::chrono::system_clock::now();
	//cpu_t = clock() - cpu_t;
	diff = t1 - t0;
	std::cout << s << "\t" << t << std::endl;
	std::cerr << *ans << std::endl;
	std::cout << (float)diff.count() * std::chrono::system_clock::period::num / std::chrono::system_clock::period::den << std::endl;
	//std::cout << ((float)cpu_t)/CLOCKS_PER_SEC << std::endl;
}

void query(po::variables_map& vm) {
  
  std::unordered_map<std::string, std::shared_ptr<PermutationStrategy>>
    perm_map{ {"Id", std::shared_ptr<PermutationStrategy>(new IdPermutationStrategy())},
      {"Degree", std::shared_ptr<PermutationStrategy>(new DegreePermutationStrategy())},
      {"FillIn", std::shared_ptr<PermutationStrategy>(new FillInPermutationStrategy())}
    };

	std::string file_name_graph(vm["graph"].as<std::string>());
	std::stringstream ss, ss_stat;
	unsigned long s = vm["source"].as<int>();
	unsigned long t = vm["target"].as<int>();
	std::string met = vm["algorithm"].as<std::string>();
	std::string semiring = vm["semiring"].as<std::string>();
	std::string strategy(vm["permutation"].as<std::string>());
	int k = vm["k"].as<int>();

	//if(argc>5) met = atoi(argv[5]);
	//if(argc>6) precomp = atoi(argv[6]);
	//if(argc>7) str = atoi(argv[7]);
	//ss << file_name_graph << "." << str << ".dec";
	Graph graph;

	if (perm_map.find(strategy)==perm_map.end()){
		std::cerr << "Unknown decomposition strategy!" << std::endl;
		exit(1);
	}
	std::cerr << "loading graph... " << std::flush;
	std::ifstream file1(file_name_graph);
	unsigned long src, tgt;
	while (file1 >> src >> tgt) {
		if (src != tgt) {
			graph.add_edge(src, tgt);
		}
	}
	file1.close();
	std::cerr << graph.number_nodes() << " nodes " << \
		graph.number_edges() << " edges" << std::endl;
	TreeDecomposition decomposition(graph, *perm_map[strategy]);
	perm_map[strategy]->init_permutation(graph);
	/* do not do this for now
	std::cerr << "loading decomposition... " << std::flush;
	std::ifstream file2(ss.str());
	if (file2) {
		file2 >> decomposition;
		file2.close();
		std::cerr << decomposition.getNumberCircuits() << " circuits" << std::endl;
	}
	else
		std::cerr << "does not exist, skipping." << std::endl;
	*/

	if 	(semiring == "TropicalSemiring") 
		queryOnSemiring<TropicalSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "SecuritySemiring") 
		queryOnSemiring<SecuritySemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "TopKSemiring")  
		queryOnSemiring<TopKSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "CountingSemiring")  
		queryOnSemiring<CountingSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "PrintingSemiring")  
		queryOnSemiring<PrintingSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "BinaryRelationSemiring")  
		queryOnSemiring<BinaryRelationSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else if (semiring == "DivisorSemiring")
		queryOnSemiring<DivisorSemiring>(decomposition, file_name_graph, k, met, s, t, *perm_map[strategy]);
	else {
		std::cerr << "Unknown semiring." << std::endl;
		exit(1);
	}
}

void decompose(po::variables_map& vm) {
	std::string file_name_graph(vm["graph"].as<std::string>());
	std::string strategy(vm["strategy"].as<std::string>());
	int part = vm["partial"].as<int>();
	std::stringstream ss, ss_stat;
	double time_sec;
	ss << file_name_graph << "." << strategy << ".dec";
	Graph graph;
	std::unique_ptr<PermutationStrategy> str = nullptr;

	if 	(strategy == "Degree")
		str = std::unique_ptr<PermutationStrategy>(new DegreePermutationStrategy());
	else if (strategy == "FillIn")
		str = std::unique_ptr<PermutationStrategy>(new FillInPermutationStrategy());
	else if (strategy == "DegreeFillIn")
		str = std::unique_ptr<PermutationStrategy>(new DegreeFillInPermutationStrategy());
	else {
		std::cerr << "Unknown decomposition strategy!" << std::endl;
		exit(1);
	}

	std::vector<std::unique_ptr<PermutationStrategy>> strategies;
	strategies.push_back(std::unique_ptr<PermutationStrategy>
		(new DegreePermutationStrategy()));
	strategies.push_back(std::unique_ptr<PermutationStrategy>
		(new FillInPermutationStrategy()));
	strategies.push_back(std::unique_ptr<PermutationStrategy>
		(new DegreeFillInPermutationStrategy()));
	std::ofstream filestat(ss_stat.str());
	std::cerr << "loading graph... " << std::flush;
	std::ifstream file1(file_name_graph);
	unsigned long src, tgt;
	while (file1 >> src >> tgt) {
		if (src != tgt) {
			graph.add_edge(src, tgt);
		}
	}
	file1.close();
	std::cerr << graph.number_nodes() << " nodes " << graph.number_edges() << \
		" edges" << std::endl;
	TreeDecomposition decomposition(graph, *str);
	std::cerr << "decomposition..." << std::flush;
	auto t0 = std::chrono::system_clock::now();
	decomposition.decompose(part);
	std::ofstream filedec(ss.str());
	filedec << decomposition;
	filedec.close();
	auto t1 = std::chrono::system_clock::now();
	auto diff = t1 - t0;
	std::cerr << " done in " << (float)diff.count() * std::chrono::system_clock::period::num / std::chrono::system_clock::period::den << " sec." << std::endl;
}

int main(int argc, const char* argv[]) {

	//program options
	po::options_description global("Parameters:");
	global.add_options()
		("help,h", "this help message")
		("command", po::value<std::string>(), "command to execute: query, decompose")
		("subargs", po::value<std::vector<std::string>>(), "arguments for command")
		;

	po::positional_options_description pos;
	pos.add("command", 1).
		add("subargs", -1);

	po::variables_map vm;

	po::parsed_options parsed = po::command_line_parser(argc, argv).
		options(global).positional(pos).allow_unregistered().run();

	po::store(parsed, vm);

	if (!vm.count("command")) {
		std::cerr << global << std::endl;
		return 1;
	}

	std::string cmd = vm["command"].as<std::string>();

	//Query processing
	if (cmd == "query") {
		po::options_description queryopt("Answers source-to-target queries on annotated graphs. Options");

		queryopt.add_options()
			("help,h", "this help message")
			("graph,g", po::value<std::string>(), "graph file name")
			("source,s", po::value<int>(), "source node id")
			("target,t", po::value<int>(), "target node id")
			("algorithm,a", po::value<std::string>()->default_value("Mohri"),
				"algorithm name (Mohri, MinPathTriangulation, NodeElimination, Dijkstra, PrecomputedProvenance, Extended Dijkstra, Access)")
			("semiring,r", po::value<std::string>()->default_value("TropicalSemiring"),
				"semiring to use (TropicalSemiring, TopKSemiring, SecuritySemiring, CountingSemiring, PrintingSemiring, BinaryRelationSemiring)")
			("permutation,p", po::value<std::string>()->default_value("Id"),
				"node permutation to use (Id, Degree, FillIn)")
			("k,k", po::value<int>()->default_value(10), "value of K (only for TopKSemiring)")
			;

		std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
		opts.erase(opts.begin());

		po::store(po::command_line_parser(opts).options(queryopt).run(), vm);

		if ((vm.count("help")) ||
			(!vm.count("graph") || !vm.count("source") || !vm.count("target"))) {
			std::cerr << queryopt << std::endl;
			return 1;
		}

		query(vm);

	}
	//Tree decomposition
	else if (cmd == "decompose")
	{

		po::options_description decopt("Created a tree decomposition. Options");

		decopt.add_options()
			("help,h", "this help message")
			("graph,g", po::value<std::string>(), "graph file name")
			("strategy,s", po::value<std::string>()->default_value("Degree"),
				"decomposition ordering strategy (Degree, FillIn, DegreeFillIn")
				("partial,p", po::value<int>()->default_value(0), "partial decomposition maximal degree")
			;

		std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
		opts.erase(opts.begin());

		po::store(po::command_line_parser(opts).options(decopt).run(), vm);

		if ((vm.count("help")) || (!vm.count("graph"))) {
			std::cerr << decopt << std::endl;
			return 1;
		}

		decompose(vm);

	}
	else {
		std::cerr << "Command unknown." << std::endl;
	}
}
