//
//  Provenance.h
//  graph-provenance
//
//  Created by Silviu Maniu on 21/07/16.
//  Copyright © 2016 Silviu Maniu. All rights reserved.
//

#ifndef Provenance_h
#define Provenance_h

#include <memory>

#include "../structure/Graph.h"
#include "../semiring/Semiring.h"


#define PROVENANCE_GATE_INTER 0
#define PROVENANCE_GATE_INPUT 1

class Provenance{
private:
  unsigned long id;
  unsigned int type;
  std::shared_ptr<Provenance> plus;
  std::shared_ptr<Provenance> times_left;
  std::shared_ptr<Provenance> times_right;
  unsigned long src;
  unsigned long tgt;
public:
  Provenance(unsigned long src, unsigned long tgt,\
             unsigned int type, unsigned long id=0)\
  : src(src), tgt(tgt), id(id), type(type) {
    plus = nullptr;
    times_left = nullptr;
    times_right = nullptr;
  };
  
  void set_plus(std::shared_ptr<Provenance> prov_ptr){
    plus = prov_ptr;
  }
  
  void set_times_left(std::shared_ptr<Provenance> prov_ptr){
    times_left = prov_ptr;
  }
  
  void set_times_right(std::shared_ptr<Provenance> prov_ptr){
    times_right = prov_ptr;
  }
  
  std::shared_ptr<Provenance> ptrPlus() {return plus;}
  
  std::shared_ptr<Provenance> ptrTimesLeft() {return times_left;}
  
  std::shared_ptr<Provenance> ptrTimesRight() {return times_right;}
  
  unsigned long source() {return src;}
  
  unsigned long target() {return tgt;}
  
  unsigned int provenanceType() {return type;}
  
  unsigned long getId() {return id;}
  
  //Output and input
  friend std::ostream& operator<<(std::ostream& out, Provenance& prov);
};

std::ostream& operator<<(std::ostream& out, Provenance& prov){
  out << prov.id << " " << prov.type << std::endl;
  out << prov.src << " " << prov.tgt << std::endl;
  unsigned long p=0, tl=0, tr=0;
  if(prov.plus!=nullptr) p = prov.plus->id;
  if(prov.times_left!=nullptr) tl = prov.times_left->id;
  if(prov.times_right!=nullptr) tr = prov.times_right->id;
  out << p << " " << tl << " " << tr << std::endl;
  return out;
}

#endif /* Provenance_h */
