#ifndef TreeDecomposition_h
#define TreeDecomposition_h

#include <vector>
#include <ostream>
#include <sstream>
#include <string>
#include <algorithm>

#include "../strategy/PermutationStrategy.h"
#include "../structure/Graph.h"
#include "../structure/Bag.h"
#include "../structure/Provenance.h"
#include "../semiring/Semiring.h"

//Class implementing a greedy tree decomposition
class TreeDecomposition{
private:
  //data structures for the tree decomposition
  Graph& graph;
  PermutationStrategy& strategy;
  std::vector<Bag> bags;
  std::unordered_map<unsigned long, unsigned long> bag_ids;
  unsigned long treewidth = 0;
  std::stringstream stat;
  
  //data structures for the triangulation index
  std::vector<unsigned long> ordering;
  std::vector<std::shared_ptr<Provenance>> circuits;
  std::unordered_map<unsigned long,\
    std::unordered_map<unsigned long, unsigned long>> circuit_id;
  std::unordered_map<unsigned long, std::unordered_set<unsigned long>> forward;
  std::unordered_map<unsigned long, std::unordered_set<unsigned long>> backward;
  std::unordered_map<unsigned long, unsigned long> pos;
  
public:
  TreeDecomposition(Graph& gr, PermutationStrategy& str):\
    graph(gr), strategy(str) {}
  
  //Computes decomposition
  unsigned long decompose(unsigned long partial_degree=0){
    //creating circuits for all the edges
    unsigned long cid = 0;
    for(auto node:graph.get_nodes()){
      for(auto neigh:graph.get_neighbours(node)){
        cid++;
        std::shared_ptr<Provenance>\
          prov_circ(new Provenance(node, neigh, PROVENANCE_GATE_INPUT, cid));
        circuits.push_back(prov_circ);
        circuit_id[node][neigh] = cid;
      }
    }
    //building the first permutation
    strategy.init_permutation(graph);
    double original_nodes = graph.number_nodes();
    double original_edges = graph.number_edges();
    unsigned long max_bag = 0;
    unsigned long bag_id = 0;
    //looping greedily through the permutation
    //we stop when the maximum bag has the same width as the remaining graph
    //or when we achive the partial decomposition condition
    while(!strategy.empty()){
      //getting the next node
      unsigned long node = strategy.get_next();
      ordering.push_back(node);
      pos[node] = ordering.size()-1;
      std::unordered_set<unsigned long> neigh = graph.get_neighbours(node);
      unsigned long width = neigh.size();
      unsigned long new_max_bag = std::max(width,max_bag);
      //we stop as soon as we find a bigger bag, in case of partial decomposition
      if(partial_degree!=0&&new_max_bag>partial_degree) break;
      max_bag=new_max_bag;
      //creating the circuits, if possible and
      //creating the forward and backward data structures
      cid = create_clique_circuits(cid, node, neigh);
      for(auto nei:neigh){
        forward[node].insert(nei);
        backward[nei].insert(node);
      }
      //removing the node from the graph
      graph.remove_node(node);
      //filling missing edges between the neighbours and recomputing statistics
      //  for relevant nodes in the graph (the neighbours, most of the time)
      graph.fill(neigh);
      strategy.recompute(neigh, graph);
      //adding the bag
      //neigh.insert(node);
      Bag bag = Bag(bag_id, neigh);
      bag.set_node(node);
      bags.push_back(bag);
      bag_ids[node] = bag_id;
      bag_id++;
    }
    stat << max_bag << "\t" << graph.number_nodes() <<"\t"\
      << graph.number_edges() << "\t" <<\
      (double)graph.number_nodes()/original_nodes << "\t" <<\
      (double)graph.number_edges()/original_edges << std::endl;
    //adding the remaining graph to the decomposition
    //Bag bag = Bag(bag_id, graph.get_nodes());
    //bags.push_back(bag);
    treewidth = std::max(max_bag,graph.number_nodes()-1);
    return treewidth;
  }

  //Computes the decomposition tree by choosing, for each bag, the neighbour
  //  that is closest in the permutation
  void build_tree(){
    for(unsigned long i=0;i<bags.size()-1;i++){
      std::vector<unsigned long> nodes = bags[i].get_nodes();
      unsigned long min_bag = bags.size()-1;
      for(unsigned long n=0;n<nodes.size()-1;n++){
        if(bag_ids.find(nodes[n])!=bag_ids.end()&&bag_ids[nodes[n]]!=i)
          min_bag = std::min(bag_ids[nodes[n]], min_bag);
      }
      bags[i].set_parent(min_bag);
      bags[min_bag].add_to_children(i);
    }
  }
  
  //Returns a string containing the statistics about the decomposition process
  std::string get_stat(){
    return stat.str();
  }
  
  //Return forward and backward positions
  std::unordered_set<unsigned long> getForward(unsigned long node){
    if(forward.find(node)!=forward.end())
      return forward[node];
    return std::unordered_set<unsigned long>();
  }
  
  std::unordered_set<unsigned long> getBackward(unsigned long node){
    if(backward.find(node)!=backward.end())
      return backward[node];
    return std::unordered_set<unsigned long>();
  }
  
  //Return position of a node
  unsigned long position(unsigned long node){
    return pos[node];
  }
  
  //Return node at position
  unsigned long nodeAtPosition(unsigned long pos){
    return ordering[pos];
  }
  
  unsigned long nodeAtLast(){
    return ordering[ordering.size()-1];
  }
  
  unsigned long getOrderingSize(){
    return ordering.size();
  }
  
  unsigned long getNumberCircuits(){
    return circuits.size();
  }
  
  //Return provenance if it exists
  std::shared_ptr<Provenance> getEdgeProvenance(unsigned long src,\
                                                unsigned long tgt){
    if(circuit_id.find(src)!=circuit_id.end()&&\
       circuit_id[src].find(tgt)!=circuit_id[src].end())
      return circuits[circuit_id[src][tgt]-1];
    else
      return nullptr;
  }
  
  std::vector<std::shared_ptr<Provenance>>& getCircuits(){
    return circuits;
  }
  
  std::vector<Bag>& getBags(){
    return bags;
  }

  Graph& getGraph() {
	  return graph;
  }

  //Output stream
  friend std::ostream& operator<<(std::ostream& out, TreeDecomposition& dec);
  
  //Input stream
  friend std::istream& operator>>(std::istream& is, TreeDecomposition& dec);

private:
  unsigned long create_clique_circuits(unsigned long cid, unsigned long node,\
                                      std::unordered_set<unsigned long>& neigh){
    unsigned long cid_in = cid; //keeping the circuit ids;
    for(auto x:neigh)
      for(auto y:neigh)
        if(x!=y){
          unsigned long c_xy = find_circuit_id(x,y);
          unsigned long c_xn = find_circuit_id(x,node);
          unsigned long c_ny = find_circuit_id(node,y);
          if(c_xn!=0&&c_ny!=0){
            //create new circuit for x->y
            cid_in++;
            std::shared_ptr<Provenance>\
            prov_circ(new Provenance(x, y, PROVENANCE_GATE_INTER, cid_in));
            if(c_xy!=0) prov_circ->set_plus(circuits[c_xy-1]);
            if(c_xn!=0&&c_ny!=0){
              prov_circ->set_times_left(circuits[c_xn-1]);
              prov_circ->set_times_right(circuits[c_ny-1]);
            }
            circuits.push_back(prov_circ);
            circuit_id[x][y] = cid_in;
          }
        }
    return cid_in;
  }
  
  unsigned long find_circuit_id(unsigned long src, unsigned long tgt){
    unsigned long retval = 0; //if not found
    if((circuit_id.find(src)!=circuit_id.end())&&\
       (circuit_id[src].find(tgt)!=circuit_id[src].end()))
       retval = circuit_id[src][tgt];
    return retval;
  }
};

std::ostream& operator<<(std::ostream& out, TreeDecomposition& dec){
  out << dec.ordering.size() << std::endl;
  for(auto node:dec.ordering){
    out << node << std::endl;
    if(dec.forward.find(node)!=dec.forward.end()){
      out << dec.forward[node].size() << " ";
      for(auto x:dec.forward[node]) out << x << " ";
    } else out << "0";
    out << std::endl;
    if(dec.backward.find(node)!=dec.backward.end()){
      out << dec.backward[node].size() << " ";
      for(auto x:dec.backward[node]) out << x << " ";
    } else out << "0";
    out << std::endl;
  }
  out << dec.circuits.size() << std::endl;
  for(auto prov:dec.circuits) out << *prov;
  return out;
}

std::istream& operator>>(std::istream& is, TreeDecomposition& dec){
  unsigned long orderingSize;
  //read the ordering
  is >> orderingSize;
  for(unsigned long i=0; i<orderingSize; i++){
    unsigned long node;
    std::unordered_set<unsigned long> bagNodeSet;
    is >> node;
    dec.ordering.push_back(node);
    dec.pos[node] = i;
    //read the forward nodes
    unsigned long fwdSize;
    is >> fwdSize;
    for(unsigned long j=0; j<fwdSize; j++){
      unsigned long fwdNode;
      is >> fwdNode;
      dec.forward[node].insert(fwdNode);
      bagNodeSet.insert(fwdNode);
    }
    //read the backward nodes;
    unsigned long backSize;
    is >> backSize;
    for(unsigned long j=0; j<backSize; j++){
      unsigned long backNode;
      is >> backNode;
      dec.backward[node].insert(backNode);
    }
    //insert bag
    Bag bag(i, bagNodeSet);
    bag.set_node(node);
    dec.bags.push_back(bag);
  }
  //read the provenance circuits
  unsigned long circuitsSize;
  is >> circuitsSize;
  for(unsigned long i=0; i<circuitsSize; i++){
    unsigned long src, tgt, id;
    unsigned int type;
    is >> id >> type >> src >> tgt;
    std::shared_ptr<Provenance> provPtr(new Provenance(src, tgt, type, id));
    dec.circuits.push_back(provPtr);
    unsigned long idPlus, idTLeft, idTRight;
    is >> idPlus >> idTLeft >> idTRight;
    //setting the circuit pointers
    if(idPlus>0) provPtr->set_plus(dec.circuits[idPlus-1]);
    if(idTLeft>0) provPtr->set_times_left(dec.circuits[idTLeft-1]);
    if(idTRight>0) provPtr->set_times_right(dec.circuits[idTRight-1]);
    dec.circuit_id[src][tgt]=id;
  }
  return is;
}

#endif /* TreeDecomposition_h */
