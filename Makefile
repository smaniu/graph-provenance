EXECUTABLE := ./provenance

CXX := g++

CXX_SRCS := $(wildcard ./*.cpp)
OBJS := ${CXX_SRCS:.cpp=.o}

INCLUDE_DIRS := ./ /usr/local/include
LIBRARY_DIRS :=  /usr/lib/x86_64-linux-gnu /usr/local/lib /usr/lib /cm/shared/apps/boost/1.66.0/lib
LIBRARIES := boost_program_options 

CPPFLAGS += -std=c++11 -W -O3

ifdef DEBUG
  CPPFLAGS += -g
endif

CPPFLAGS += $(foreach includedir,$(INCLUDE_DIRS),-I$(includedir))
LDFLAGS += $(foreach librarydir,$(LIBRARY_DIRS),-L$(librarydir))
LDLIBS += $(foreach library,$(LIBRARIES),-l$(library))

.PHONY: all clean

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(LINK.cc) $(OBJS) $(LDLIBS) -o $(EXECUTABLE)

main.o: main.cpp $(wildcard */*.h)

clean:
	@- $(RM) $(EXECUTABLE)
	@- $(RM) $(OBJS)
