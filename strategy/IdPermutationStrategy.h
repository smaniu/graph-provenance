#ifndef IdPermutationStrategy_h
#define IdPermutationStrategy_h

#include "PermutationStrategy.h"

class IdPermutationStrategy: public PermutationStrategy{
public:
  unsigned long compute_statistic(unsigned long node, Graph& graph,\
                                  bool init=false){
	  return node;
  }
};


#endif /* IdPermutationStrategy_h */
